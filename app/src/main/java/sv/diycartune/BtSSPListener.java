package sv.diycartune;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import java.util.UUID;

/**
 * Created by shulika on 17.09.2015.
 * Я слышу и забываю. Я вижу и запоминаю. Я делаю и понимаю. (Кун-цзы)
 */

//============================================================================================
public class BtSSPListener {

    class BtSocketListener implements Runnable {

        private Handler handler;
        public String address = ""; // 00:12:03:10:22:16 - my BTtoUART
        private BluetoothAdapter btAdapter = null;
        //private BluetoothSocket btSocket = null;
        volatile public Set<BluetoothDevice> bondedDevices;

        /*volatile (изменчивый, не постоянный). Его необходимо использовать для переменных,
        которые используются разными потоками. Это связано с тем, что значение переменной,
        объявленной без volatile, может кэшироваться отдельно для каждого потока,
        и значение из этого кэша может различаться для каждого из них.
        Объявление переменной с ключевым словом volatile отключает для неё такое кэширование
        и все запросы к переменной будут направляться непосредственно в память.*/

        //------------------------------------------------------------------------------------------
        public BtSocketListener(/*BluetoothSocket socket,*/Handler handler ) {
            //this.socket = socket;
            this.handler = handler;
            this.btAdapter = BluetoothAdapter.getDefaultAdapter();
            if(btAdapter!=null)
                this.bondedDevices = btAdapter.getBondedDevices();
        }
        //------------------------------------------------------------------------------------------
        private void SendDataToMainAPP(String m) {
            Message msg = new Message();
            msg.obj = m;
            handler.sendMessage(msg);
            //SendMsg(m);

        }
        //------------------------------------------------------------------------------------------
        private void SendMsg(String m) {
            Log.d("BtListenerThreadMsg", m);
        }
        //------------------------------------------------------------------------------------------
        private boolean BTStateOK() {
            // Check for Bluetooth support and then check to make sure it is turned on
            if(btAdapter==null) {
                SendMsg("Fatal Error. Bluetooth Not supported. Aborting.");
            } else {
                if (btAdapter.isEnabled()) {
                    SendMsg("\n...Bluetooth is enabled...");
                    return true;
                } else {
                    btAdapter.enable();
                    if(btAdapter.isEnabled())
                        return true;
                }
            }
            SendMsg("Fatal Error. Bluetooth Not start. Aborting.");
            return false;
        }
        //-------------------------------------------------------------------------------------------

        public void run() {

            BluetoothSocket btSocket = null;
            BluetoothDevice device = null;
            UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

            if(!BTStateOK()) {return;}

            if(btSocket==null){
                if(address==""){
                    SendMsg("Need client address.");
                    return;
                }

                device = btAdapter.getRemoteDevice(address);

                try {
                    btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
                } catch (IOException e) {
                    SendMsg("Fatal Error. Socket create failed: " + e.getMessage() + ".");
                    return;
                }

                btAdapter.cancelDiscovery();
            }

            try {
                btSocket.connect();
                SendMsg("\n...Connection to " + address + " established and data link opened...");
            } catch (IOException e) {
                try {
                    btSocket.close();
                } catch (IOException e2) {
                    SendMsg("Fatal Error. In btSocket.connect()" + e2.getMessage() + ".");
                    return;
                }
            }

            int bufferSize = 1024;
            int byteAvable;
            byte[] buffer = new byte[bufferSize];
            try {
                InputStream instream = btSocket.getInputStream();
                int bytesRead = -1;
                String message = "";

                while (!Thread.interrupted()) {

                    byteAvable = instream.available();

                    if(byteAvable>0)
                    {
                        try {Thread.sleep(100);} catch (InterruptedException e) {}
                        bytesRead = instream.read(buffer);

                        SendMsg(new String(buffer, 0, bytesRead - 1));

                        message = message + new String(buffer, 0, bytesRead - 1);

                        if(message.contains("\r")) {
                            SendDataToMainAPP(message);
                            message = "";
                        }
                    }
                }


                /*
                while (!Thread.interrupted()) {
                    bytesRead = -1;
                    //message = "";
                    byteAvable = instream.available();

                    if(byteAvable>0) {
                        try {
                            Thread.sleep(100);
                        }
                        catch (InterruptedException e) {}

                        bytesRead = instream.read(buffer);
                    }

                    if (bytesRead != -1)
                    {
                        SendMsg(new String(buffer, 0, bytesRead - 1));

                        while ((bytesRead==bufferSize)&&(buffer[bufferSize-1] != 0))
                        {
                            message = message + new String(buffer, 0, bytesRead);
                            bytesRead = instream.read(buffer);
                        }

                        message = message + new String(buffer, 0, bytesRead - 1);

                        //SendMsg(new String(buffer, 0, bytesRead - 1));

                        if(message.contains("\r")) {
                            SendDataToMainAPP(message);
                            message = "";
                        }
                    }
                }*/

                if (instream != null) {
                    try {instream.close();} catch (Exception e) {}
                    instream = null;
                }

                if (btSocket != null) {
                    try {btSocket.close();} catch (Exception e) {}
                    btSocket = null;
                }
                SendMsg("SocketClose");


            } catch (IOException e) {
                SendMsg("BLUETOOTH_COMMS, " + e.getMessage() + ".");
                //this.AppClose = false;
            }
        }
    }

    //========================================================================================

    public Thread BtSocketListenerThread = null;
    BtSocketListener btSocketListener;
    private static final String TAG = "BtSSPListener";

    //-----------------------------------------------------------------------------------------
    public BtSSPListener(Handler handler) {
        btSocketListener = new BtSocketListener(handler);
    }
    //-----------------------------------------------------------------------------------------

    private void BtSocketListenerThreadStart (){

        BtSocketListenerThread = new Thread(btSocketListener, "BluetoothSocketListener thread");
        BtSocketListenerThread.setDaemon(true);
        BtSocketListenerThread.start();

    }

    //-----------------------------------------------------------------------------------------

    public void Start() {

        if(BtSocketListenerThread!=null) {
            if (!BtSocketListenerThread.isAlive()) {
                BtSocketListenerThreadStart();
            } else {

                Stop();

                if (!BtSocketListenerThread.isAlive()) {
                    BtSocketListenerThreadStart();
                }
            }
        }
            else{
                BtSocketListenerThreadStart();

        }
    }

    //-----------------------------------------------------------------------------------------

    public void Stop() {

        //btSocketListener.AppClose = true;
        try {
            //BtSocketListenerThread.join(200);
            BtSocketListenerThread.interrupt();
        } catch (Exception e)
        {
            Log.d(TAG, "Fatal Error In  BtSSPListener.Stop(), thread don't stop : " + e.getMessage());
        }

    }

    //-----------------------------------------------------------------------------------------

}
