package sv.diycartune;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.bluetooth.BluetoothDevice;
import android.widget.TextView;

//================================================================================================

public class MainActivity extends AppCompatActivity {

    TextView out;
    private static final String TAG = "DiyCarTune";

    private Handler handler;
    BtSSPListener btSSPListener;



//=================================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Log.d(TAG, "onCreate()");
        setContentView(R.layout.activity_main);
        out = (TextView) findViewById(R.id.out);


        // Приложение запущено впервые или восстановлено из памяти?
        if ( savedInstanceState == null )   // приложение запущено впервые
        {
            handler = new Handler()
            {
                @Override
                public void handleMessage(Message msg)
                {
                    String text = (String) msg.obj;

                    text = text.replaceAll(" ", "\r\n");

                    out.setText(text);
                }
            };

            btSSPListener = new BtSSPListener (handler);
        }
    }

//=================================================================================================
/*
    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart()");
    }
*/
//=================================================================================================
/*
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");

    }
*/
//=================================================================================================

    @Override
    public void onResume() {
        super.onResume();
        //Log.d(TAG, "onResume()");

        btSSPListener.Start(); // запуск прослушивания данных от сканера штрихкодов
    }
//=================================================================================================


    @Override
    public void onPause() {
        super.onPause();
        //Log.d(TAG, "onPause()");

        btSSPListener.Stop(); // остановка прослушивания
    }
//=================================================================================================

/*
    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop()");
    }
*/
//=================================================================================================

/*
    public void AlertBox( String title, String message ){
        new AlertDialog.Builder(this)
                .setTitle( title )
                .setMessage( message + " Press OK to exit." )
                .setPositiveButton("OK", new OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        finish();
                    }
                }).show();
    }*/

    //-------------------------------------------------------------------------------------------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        for (BluetoothDevice device : btSSPListener.btSocketListener.bondedDevices) {
            // просто забиваю меню мак адресами спаренных устройств чтоб можно было выбрать
            menu.add(Menu.NONE, device.hashCode(), Menu.NONE, device.toString());

        }
        return true;
    }

//-------------------------------------------------------------------------------------------------


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //int id = item.getItemId();

        // выбираем конкретное устройство для подключения передав его мак адрес
        btSSPListener.btSocketListener.address = item.getTitle().toString();
        // запускаем прослушивание
        btSSPListener.Start();

        return super.onOptionsItemSelected(item);
    }

}